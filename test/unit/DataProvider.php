<?php

namespace Test\Unit;

use Samy\Dummy\Random;

class DataProvider
{
    private function csv(string $Basename): array
    {
        $ret = array();

        $filename = __DIR__ . DIRECTORY_SEPARATOR . $Basename . ".csv";

        if (is_file($filename)) {
            $file = fopen($filename, "r");

            if ($file) {
                while (($data = fgetcsv($file)) !== false) {
                    array_push($ret, $data);
                }

                fclose($file);
            }
        }

        return $ret;
    }


    // $Data
    public function dataCreate(): array
    {
        $ret = array();

        foreach ($this->csv("create") as $data) {
            $count = count($data);

            array_push($ret, array(
                array(
                    "id" => ($count > 0 ? intval($data[0]) : 0),
                    "name" => ($count > 1 ? $data[1] : ""),
                    "animal" => ($count > 2 ? $data[2] : ""),
                    "color" => ($count > 3 ? $data[3] : ""),
                    "gender" => ($count > 4 ? intval($data[4]) : 0),
                    "price" => ($count > 5 ? intval($data[5]) : 0),
                    "create" => "NOW()",
                    "update" => "NOW()"
                )
            ));
        }

        return $ret;
    }

    // $Clause, $IsExists, $Id, $Name, $Animal, $Color, $Gender, $Price
    public function dataRead(): array
    {
        $ret = array();
        $random = new Random();

        foreach ($this->csv("create") as $data) {
            $count = count($data);
            $id = ($count > 0 ? intval($data[0]) : 0);

            array_push($ret, array(
                array("id" => $id), true, $id,
                ($count > 1 ? $data[1] : ""),
                ($count > 2 ? $data[2] : ""),
                ($count > 3 ? $data[3] : ""),
                ($count > 4 ? intval($data[4]) : 0),
                ($count > 5 ? intval($data[5]) : 0)
            ));

            array_push($ret, array(
                array("id" => $random->integer(100)), false,
                0, "", "", "", 0, 0
            ));
        }

        return $ret;
    }

    // $Clause, $IsExists, $Id, $Name, $Animal, $Color, $Gender, $Price
    public function dataUpdate(): array
    {
        $ret = array();
        $random = new Random();

        foreach ($this->csv("update") as $data) {
            $count = count($data);
            $id = ($count > 0 ? intval($data[0]) : 0);

            array_push($ret, array(
                array("id" => $id), true, $id,
                ($count > 1 ? $data[1] : ""),
                ($count > 2 ? $data[2] : ""),
                ($count > 3 ? $data[3] : ""),
                ($count > 4 ? intval($data[4]) : 0),
                ($count > 5 ? intval($data[5]) : 0)
            ));

            array_push($ret, array(
                array("id" => $random->integer(100)), false,
                0, "", "", "", 0, 0
            ));
        }

        return $ret;
    }

    // $Clause, $IsExists
    public function dataDelete(): array
    {
        $random = new Random();

        return array(
            array(array("id" => $random->integer(100)), false),
            array(array("id" => 2), true),
            array(array("price > 1900"), true)
        );
    }
}
