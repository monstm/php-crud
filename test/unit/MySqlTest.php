<?php

namespace Test\Unit;

use Samy\Crud\MySql;
use Samy\Sql\MySql as SqlMySql;

class MySqlTest extends AbstractCrud
{
    protected function setUp(): void
    {
        $this->crud = new MySql(
            MYSQL_HOST,
            MYSQL_USERNAME,
            MYSQL_PASSWORD,
            MYSQL_DATABASE,
            intval(MYSQL_PORT)
        );
    }


    public function testSqlInit(): void
    {
        $mysql = new SqlMySql(
            MYSQL_HOST,
            MYSQL_USERNAME,
            MYSQL_PASSWORD,
            MYSQL_DATABASE,
            intval(MYSQL_PORT)
        );

        $is_connected = $mysql->isConnected();

        $this->assertTrue($is_connected);

        if ($is_connected) {
            $commands = array(
                "DROP TABLE IF EXISTS `" . $this->table_name . "`",
                "CREATE TABLE `" . $this->table_name . "`(" .
                    "`id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY, " .
                    "`name` varchar(24) NOT NULL, " .
                    "`animal` varchar(36) NOT NULL, " .
                    "`color` varchar(16) NOT NULL, " .
                    "`gender` tinyint unsigned NOT NULL, " .
                    "`price` smallint unsigned NOT NULL, " .
                    "`create` datetime NOT NULL, " .
                    "`update` datetime NOT NULL" .
                    ")"
            );

            foreach ($commands as $command) {
                $mysql->execute($command);
            }
        }
    }
}
