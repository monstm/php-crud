<?php

namespace Test\Unit;

use Samy\Crud\PostgreSql;
use Samy\Sql\PostgreSql as SqlPostgreSql;

class PostgreSqlTest extends AbstractCrud
{
    protected function setUp(): void
    {
        $this->crud = new PostgreSql(
            POSTGRESQL_HOST,
            POSTGRESQL_USERNAME,
            POSTGRESQL_PASSWORD,
            POSTGRESQL_DATABASE,
            intval(POSTGRESQL_PORT)
        );
    }


    public function testSqlInit(): void
    {
        $mysql = new SqlPostgreSql(
            POSTGRESQL_HOST,
            POSTGRESQL_USERNAME,
            POSTGRESQL_PASSWORD,
            POSTGRESQL_DATABASE,
            intval(POSTGRESQL_PORT)
        );

        $is_connected = $mysql->isConnected();

        $this->assertTrue($is_connected);

        if ($is_connected) {
            $commands = array(
                "DROP TABLE IF EXISTS " . $this->table_name . " CASCADE",
                "CREATE TABLE " . $this->table_name . "(" .
                    "\"id\" serial NOT NULL, " .
                    "PRIMARY KEY (\"id\"), " .
                    "\"name\" character varying(24) NOT NULL, " .
                    "\"animal\" character varying(36) NOT NULL, " .
                    "\"color\" character varying(16) NOT NULL, " .
                    "\"gender\" smallint NOT NULL, " .
                    "\"price\" smallint NOT NULL, " .
                    "\"create\" timestamp NOT NULL, " .
                    "\"update\" timestamp NOT NULL" .
                    ")"
            );

            foreach ($commands as $command) {
                $mysql->execute($command);
            }
        }
    }
}
