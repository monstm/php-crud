<?php

namespace Test\Unit;

use PHPUnit\Framework\TestCase;
use Samy\Crud\AbstractCrud as CrudAbstractCrud;

class AbstractCrud extends TestCase
{
    protected $crud;

    protected $table_name = "php_crud_pet";
    protected $data_fieds = array("id", "name", "animal", "color", "gender", "price");
    protected $unique_field = "name";


    /**
     * @depends testSqlInit
     * @dataProvider \Test\Unit\DataProvider::dataCreate
     */
    public function testCreate($Data): void
    {
        $this->assertInstanceOf(
            CrudAbstractCrud::class,
            $this->crud
                ->withSchema($this->table_name, $this->data_fieds, $this->unique_field)
                ->create($Data)
        );

        $this->assertTrue($this->crud->isExists());
    }

    /**
     * @depends testCreate
     * @dataProvider \Test\Unit\DataProvider::dataRead
     */
    public function testRead($Clause, $IsExists, $Id, $Name, $Animal, $Color, $Gender, $Price): void
    {
        $this->assertInstanceOf(
            CrudAbstractCrud::class,
            $this->crud
                ->withSchema($this->table_name, $this->data_fieds, $this->unique_field)
                ->withRecord($Clause)
        );

        $this->assertSame($IsExists, $this->crud->isExists());

        $this->assertSame(
            ($IsExists ?
                array(
                    "id" => $Id,
                    "name" => $Name,
                    "animal" => $Animal,
                    "color" => $Color,
                    "gender" => $Gender,
                    "price" => $Price
                ) :
                array()
            ),
            $this->crud->getData()
        );

        $this->assertSame($Id, $this->crud->getValue("id", 0));
        $this->assertSame($Name, $this->crud->getValue("name", ""));
        $this->assertSame($Animal, $this->crud->getValue("animal", ""));
        $this->assertSame($Color, $this->crud->getValue("color", ""));
        $this->assertSame($Gender, $this->crud->getValue("gender", 0));
        $this->assertSame($Price, $this->crud->getValue("price", 0));
    }

    /**
     * @depends testRead
     * @dataProvider \Test\Unit\DataProvider::dataUpdate
     */
    public function testUpdate($Clause, $IsExists, $Id, $Name, $Animal, $Color, $Gender, $Price): void
    {
        $this->assertInstanceOf(
            CrudAbstractCrud::class,
            $this->crud
                ->withSchema($this->table_name, $this->data_fieds, $this->unique_field)
                ->withRecord($Clause)
        );

        $this->assertSame($IsExists, $this->crud->isExists());

        $this->assertInstanceOf(CrudAbstractCrud::class, $this->crud->update(array(
            "name" => $Name,
            "animal" => $Animal,
            "color" => $Color,
            "gender" => $Gender,
            "price" => $Price
        )));

        if ($IsExists) {
            $this->assertSame(
                array(
                    "id" => $Id,
                    "name" => $Name,
                    "animal" => $Animal,
                    "color" => $Color,
                    "gender" => $Gender,
                    "price" => $Price
                ),
                $this->crud->getData()
            );

            $this->assertSame($Id, $this->crud->getValue("id", 0));
            $this->assertSame($Name, $this->crud->getValue("name", ""));
            $this->assertSame($Animal, $this->crud->getValue("animal", ""));
            $this->assertSame($Color, $this->crud->getValue("color", ""));
            $this->assertSame($Gender, $this->crud->getValue("gender", 0));
            $this->assertSame($Price, $this->crud->getValue("price", 0));
        } else {
            $this->assertSame(array(), $this->crud->getData());
            $this->assertSame(0, $this->crud->getValue("id", 0));
            $this->assertSame("", $this->crud->getValue("name", ""));
            $this->assertSame("", $this->crud->getValue("animal", ""));
            $this->assertSame("", $this->crud->getValue("color", ""));
            $this->assertSame(0, $this->crud->getValue("gender", 0));
            $this->assertSame(0, $this->crud->getValue("price", 0));
        }
    }

    /**
     * @depends testRead
     * @dataProvider \Test\Unit\DataProvider::dataDelete
     */
    public function testDelete($Clause, $IsExists): void
    {
        $this->assertInstanceOf(
            CrudAbstractCrud::class,
            $this->crud
                ->withSchema($this->table_name, $this->data_fieds, $this->unique_field)
                ->withRecord($Clause)
        );

        $this->assertSame($IsExists, $this->crud->isExists());

        $this->assertInstanceOf(CrudAbstractCrud::class, $this->crud->delete());

        $this->assertFalse($this->crud->isExists());
    }
}
