<?php

namespace Samy\Crud;

use Samy\Sql\MySql as SqlMySql;

/**
 * Simple MySQL implementation.
 */
class MySql extends AbstractCrud
{
    /**
     * MySql construction.
     *
     * @param[in] string $Host MySQL Host
     * @param[in] string $Username MySQL Username
     * @param[in] string $Password MySQL Password
     * @param[in] string $Database MySQL Database
     * @param[in] int $Port MySQL Port
     *
     * @return void
     */
    public function __construct(string $Host, string $Username, string $Password, string $Database, int $Port = 3306)
    {
        $this->driver = new SqlMySql($Host, $Username, $Password, $Database, $Port);
    }
}
