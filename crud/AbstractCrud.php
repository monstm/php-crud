<?php

namespace Samy\Crud;

/**
 * This is a simple CRUD implementation that other CRUD can inherit from.
 */
abstract class AbstractCrud implements CrudInterface
{
    /** describe driver */
    protected $driver = null;

    /** describe table name */
    protected $table = "";

    /** describe fields */
    protected $fields = array();

    /** describe unique field */
    protected $unique_field = "";

    /** describe schema value */
    protected $unique_value = null;

    /** describe data */
    protected $data = array();


    /**
     * Return an instance with provided schema
     *
     * @param[in] string $TableName Table name
     * @param[in] array $DataFields Data fields
     * @param[in] string $UniqueField Unique field name
     *
     * @return static
     */
    public function withSchema(string $TableName, array $DataFields, string $UniqueField): self
    {
        $this->table = $TableName;
        $this->fields = $DataFields;

        $this->unique_field = $UniqueField;
        $this->unique_value = null;

        return $this;
    }

    /**
     * Return an instance with record
     *
     * @param[in] array $Clause SQL clause
     *
     * @return static
     */
    public function withRecord(array $Clause): self
    {
        $this->data = $this->driver
            ->table($this->table)
            ->record($this->fields, $Clause);

        $this->unique_value = ($this->data[$this->unique_field] ?? null);

        return $this;
    }


    /**
     * Check if record is exists
     *
     * @return bool
     */
    public function isExists(): bool
    {
        $clause = array();
        $clause[$this->unique_field] = $this->unique_value;

        return ($this->driver
            ->table($this->table)
            ->count($clause) > 0
        );
    }


    /**
     * Retrieve all data
     *
     * @return array<string, mixed>
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * Retrieve data value
     *
     * @param[in] string $FieldName Field name
     * @param[in] mixed $Default Default value
     *
     * @return mixed
     */
    public function getValue(string $FieldName, mixed $Default = null): mixed
    {
        return ($this->data[$FieldName] ?? $Default);
    }


    /**
     * Return an instance with created data
     *
     * @param[in] array $Data Create data
     *
     * @return static
     */
    public function create(array $Data): self
    {
        $affected_rows = $this->driver
            ->table($this->table)
            ->insert($Data)
            ->executeAffectedRows();

        if ($affected_rows > 0) {
            if (isset($Data[$this->unique_field])) {
                $clause = array();
                $clause[$this->unique_field] = $Data[$this->unique_field];

                $this->withRecord($clause);
            }
        }

        return $this;
    }

    /**
     * Return an instance with updated data
     *
     * @param[in] array $Data Data update
     *
     * @return static
     */
    public function update(array $Data): self
    {
        if ($this->unique_value != null) {
            $clause = array();
            $clause[$this->unique_field] = $this->unique_value;

            $this->driver
                ->table($this->table)
                ->update($Data, $clause);


            if (isset($Data[$this->unique_field])) {
                $clause[$this->unique_field] = $Data[$this->unique_field];
            }

            $this->withRecord($clause);
        }

        return $this;
    }

    /**
     * Return an instance with deleted data
     *
     * @return static
     */
    public function delete(): self
    {
        if ($this->unique_value != null) {
            $clause = array();
            $clause[$this->unique_field] = $this->unique_value;

            $this->driver
                ->table($this->table)
                ->delete($clause);

            $this->withRecord($clause);
        }

        return $this;
    }
}
