<?php

namespace Samy\Crud;

use Samy\Sql\PostgreSql as SqlPostgreSql;

/**
 * Simple PostgreSQL implementation.
 */
class PostgreSql extends AbstractCrud
{
    /**
     * PostgreSql construction.
     *
     * @param[in] string $Host PostgreSQL Host
     * @param[in] string $Username PostgreSQL Username
     * @param[in] string $Password PostgreSQL Password
     * @param[in] string $Database PostgreSQL Database
     * @param[in] int $Port PostgreSQL Port
     *
     * @return void
     */
    public function __construct(string $Host, string $Username, string $Password, string $Database, int $Port = 5432)
    {
        $this->driver = new SqlPostgreSql($Host, $Username, $Password, $Database, $Port);
    }
}
