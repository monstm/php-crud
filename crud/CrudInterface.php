<?php

namespace Samy\Crud;

/**
 * Describes CRUD interface.
 */
interface CrudInterface
{
    /**
     * Return an instance with provided schema.
     *
     * @param[in] string $TableName Table name
     * @param[in] array $DataFields Data fields
     * @param[in] string $UniqueField Unique field name
     *
     * @return static
     */
    public function withSchema(string $TableName, array $DataFields, string $UniqueField): self;

    /**
     * Return an instance with record.
     *
     * @param[in] array $Clause SQL clause
     *
     * @return static
     */
    public function withRecord(array $Clause): self;


    /**
     * Check if record is exists.
     *
     * @return bool
     */
    public function isExists(): bool;


    /**
     * Retrieve all data.
     *
     * @return array<string, mixed>
     */
    public function getData(): array;

    /**
     * Retrieve data value.
     *
     * @param[in] string $FieldName Field name
     * @param[in] mixed $Default Default value
     *
     * @return mixed
     */
    public function getValue(string $FieldName, mixed $Default = null): mixed;


    /**
     * Return an instance with created data.
     *
     * @param[in] array $Data Create data
     *
     * @return static
     */
    public function create(array $Data): self;

    /**
     * Return an instance with updated data.
     *
     * @param[in] array $Data Data update
     *
     * @return static
     */
    public function update(array $Data): self;

    /**
     * Return an instance with deleted data.
     *
     * @return static
     */
    public function delete(): self;
}
