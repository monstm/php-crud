# PHP CRUD

[
	![](https://badgen.net/packagist/v/samy/crud/latest)
	![](https://badgen.net/packagist/license/samy/crud)
	![](https://badgen.net/packagist/dt/samy/crud)
	![](https://badgen.net/packagist/favers/samy/crud)
](https://packagist.org/packages/samy/crud)

Create, Read, Update, and Delete (CRUD) are the four basic functions that models should be able to do, at most.

---

## Instalation

### Composer

Composer is a tool for dependency management in PHP.
It allows you to declare the libraries your project depends on and it will manage (install/update) them for you.

```sh
composer require samy/crud
```

Please see [Composer](https://getcomposer.org/) for more information.

---

## Support

* Repository: <https://gitlab.com/monstm/php-crud>
* Documentations: <https://monstm.gitlab.io/php-crud/>
* Annotation: <https://monstm.alwaysdata.net/php-crud/>
* Issues: <https://gitlab.com/monstm/php-crud/-/issues>
