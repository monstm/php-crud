# CRUD Interface

Describes CRUD interface.

---

## withSchema

Return an instance with provided schema.

```php
$crud = $crud->withSchema($table_name, $data_fields, $unique_field);
```

---

## withRecord

Return an instance with record.

```php
$crud = $crud->withRecord($clause);
```

---

## isExists

Check if record is exists.

```php
$is_exists = $crud->isExists();
```

---

## getData

Retrieve all data.

```php
$data = $crud->getData();
```

---

## getValue

Retrieve data value.

```php
$value = $crud->getValue($field_name, $default);
```

---

## create

Return an instance with created data.

```php
$crud = $crud->create($data);
```

---

## update

Return an instance with updated data.

```php
$crud = $crud->update($data);
```

---

## delete

Return an instance with deleted data.

```php
$crud = $crud->delete();
```
