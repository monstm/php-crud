# CRUD Instance

Simple CRUD Implementation.

---

## MySQL

Implementation MySQL instance.

```php
$crud = new MySql($host, $username, $password, $database, $port = 3306);
```

---

## PostgreSQL

Implementation PostgreSQL instance.

```php
$crud = new PostgreSql($host, $username, $password, $database, $port = 5432);
```
